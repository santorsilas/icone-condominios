(function($) {
    "use strict"; // Start of use strict

    var type = '';
    var officials = '';

	$('#phone').mask('(00) 00000-0000');
    $('#phone2').mask('(00) 00000-0000');
    $('#postal').mask('00000-000');
    $('#rate').mask("#.##0,00", {reverse: true});

    $('#responseEmp1').change(function(){
    	$('#response').show();
    });

    $('#responseEmp2').change(function(){
    	$('#response').hide();
    });


    $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); 

            // dados do cliente
            var name = $("input#name").val();
            var phone = $("input#phone").val();
            var email = $("input#email").val();
            var profile = $('input[name=profile]:checked').val();

            // dados do condominio
            var condominium = $("#condominium").val();
            var address = $("#address").val();
            var neighborhood = $("#neighborhood").val();
            var postal = $("#postal").val();
            var city = $("#city").val();
            var phone2 = $("#phone2").val();
            var type = $('input[name=type]:checked').val();
            var qtdType = $("#qtdType").val();
            var officials = $('input[name=officials]:checked').val();
            var qtdOfficials = $("#qtdOfficials").val();
            var rate = $("#rate").val();
            var reason = $("#reason").val() != '' ? $("#reason").val() : "Não possui empresa.";
            var time = $("#time").val();
            var note = $("#note").val();

            var firstName = name; 

            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            
            $.ajax({
                url: "./mail/proposta.php",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    profile: profile,

                    condominium: condominium,
                    address: address,
                    neighborhood: neighborhood,
                    postal: postal,
                    city: city,
                    type: type,
                    qtdType: qtdType,
                    officials: officials,
                    qtdOfficials: qtdOfficials,
                    rate: rate,
                    reason: reason,
                    time: time,
                    note: note
                },
                cache: false,
                success: function(data) {

                    console.log(data);
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Sua mensagem foi enviada com sucesso.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Desculpe " + firstName + ", mas meu servidor de email não está respondendo. Por favor, tente novamente mais tarde!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },

        submitError: function ($form, event, errors) { 
        	console.log(errors);
        }
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
})(jQuery); 

$('#name').focus(function() {
    $('#success').html('');
});