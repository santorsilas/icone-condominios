<?php
/* apenas dispara o envio do formulário caso exista $_POST['enviarFormulario']*/
/*
// Check for empty fields
if(empty($_POST['name'])         ||
   empty($_POST['email'])        ||
   empty($_POST['phone'])        ||
   empty($_POST['profile'])      ||
   empty($_POST['condominium'])  ||
   empty($_POST['address'])      ||
   empty($_POST['neighborhood']) ||
   empty($_POST['city'])         ||
   empty($_POST['type'])         ||
   empty($_POST['officials'])    ||
   empty($_POST['qtdOfficials']) ||
   empty($_POST['qtdType'])      ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
  	echo "No arguments Provided!";
   return false;
}
/*
 

/*** INÍCIO - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/
 
$enviaFormularioParaNome = 'Cristiano';
$enviaFormularioParaEmail = 'cristiano@iconecondominios.com.br';
 
$caixaPostalServidorNome = 'site';
$caixaPostalServidorEmail = 'site@iconecondominios.com.br';
$caixaPostalServidorSenha = 'conta007!';
 
/*** FIM - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/ 
 
 
/* abaixo as veriaveis principais, que devem conter em seu formulario*/
 
$remetenteNome = strip_tags(htmlspecialchars($_POST['name']));
$remetenteEmail = strip_tags(htmlspecialchars($_POST['email']));
$profile = strip_tags(htmlspecialchars($_POST['profile']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));

$condominium = strip_tags(htmlspecialchars($_POST['condominium']));
$address = strip_tags(htmlspecialchars($_POST['address']));
$neighborhood = strip_tags(htmlspecialchars($_POST['neighborhood']));
$postal = strip_tags(htmlspecialchars($_POST['postal']));
$city = strip_tags(htmlspecialchars($_POST['city']));
$phone2 = strip_tags(htmlspecialchars($_POST['phone2']));
$type = strip_tags(htmlspecialchars($_POST['type']));
$qtdType = strip_tags(htmlspecialchars($_POST['qtdType']));
$officials = strip_tags(htmlspecialchars($_POST['officials']));
$qtdOfficials = strip_tags(htmlspecialchars($_POST['qtdOfficials']));
$rate = strip_tags(htmlspecialchars($_POST['rate']));
$reason = strip_tags(htmlspecialchars($_POST['reason']));
$time = strip_tags(htmlspecialchars($_POST['time']));
$note = strip_tags(htmlspecialchars($_POST['note']));

$assunto = "Proposta " . $condominium;

$mensagemConcatenada = 'Formulário gerado via website'.'<br/>'; 
$mensagemConcatenada .= '-------------------------------<br/><br/>'; 
$mensagemConcatenada .= 'Nome: '.$remetenteNome.'<br/>'; 
$mensagemConcatenada .= 'E-mail: '.$remetenteEmail.'<br/>'; 
$mensagemConcatenada .= 'Telefone: '.$assunto.'<br/>';
$mensagemConcatenada .= 'Perfil: '.$profile.'<br/>';
$mensagemConcatenada .= '-------------------------------<br/><br/>'; 
$mensagemConcatenada .= 'Condomínio: '.$condominium.'<br/>';
$mensagemConcatenada .= 'Endereço: '.$address.'<br/>';
$mensagemConcatenada .= 'Bairro: '.$neighborhood.'<br/>';
$mensagemConcatenada .= 'CEP: '.$postal.'<br/>';
$mensagemConcatenada .= 'Cidade: '.$city.'<br/>';
$mensagemConcatenada .= 'Telefone: '.$phone2.'<br/>';
$mensagemConcatenada .= 'Tipo: '.$type.'<br/>';
$mensagemConcatenada .= 'Qtd. de unidades: '.$qtdType.'<br/>';
$mensagemConcatenada .= 'Tipo de Funcionários: '.$officials.'<br/>';
$mensagemConcatenada .= 'Qtd. de Funcionários: '.$qtdOfficials.'<br/>';
$mensagemConcatenada .= 'Taxa condominial: R$ '.$rate.'<br/>';
$mensagemConcatenada .= 'Motivo para substituir empresa atual: '.$reason.'<br/>';
$mensagemConcatenada .= 'Tempo para fechar cotação: '.$time.'<br/>';
$mensagemConcatenada .= '-------------------------------<br/><br/>'; 
$mensagemConcatenada .= 'Obervações: '.$note.'<br/>';
 
/*********************************** A PARTIR DAQUI NAO ALTERAR ************************************/ 
 
require_once('mailer/PHPMailerAutoload.php');
 
$mail = new PHPMailer();
 
$mail->IsSMTP();
$mail->SMTPAuth  = true;
$mail->Charset   = 'utf8_decode()';
$mail->Host  = 'smtp.'.substr(strstr($caixaPostalServidorEmail, '@'), 1);
$mail->Port  = '587';
$mail->Username  = $caixaPostalServidorEmail;
$mail->Password  = $caixaPostalServidorSenha;
$mail->From  = $caixaPostalServidorEmail;
$mail->FromName  = utf8_decode($caixaPostalServidorNome);
$mail->IsHTML(true);
$mail->Subject  = utf8_decode($assunto);
$mail->Body  = utf8_decode($mensagemConcatenada);
 
 
$mail->AddAddress($enviaFormularioParaEmail,utf8_decode($enviaFormularioParaNome));
 
if(!$mail->Send()){
	echo 'Erro ao enviar formulário: '. print($mail->ErrorInfo);
	return false;
}else{
	return true;
} 
